'use strict';

/**
 * Definination of modules, configs etc..
 */

angular.module('betterPOSClientApp', [
  'ui.bootstrap',
  'ui.router',
  'betterPOSClientApp.controller.main',
  'betterPOSClientApp.controller.menu',
  'betterPOSClientApp.controller.foodDesc',
  'betterPOSClientApp.controller.cart',
  'betterPOSClientApp.controller.auth',
  'betterPOSClientApp.service.cart',
  'betterPOSClientApp.filters'
  // 'betterPOSClientApp.directives'
]).

config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/home/rice");

  $stateProvider
    .state("home", {
      url: "/home",
      templateUrl: "src/view/home.html"
    })

    .state("home.rice", {
      url: "/rice",
      templateUrl: "src/view/partials/menu.html"
    })

    .state("home.noodle", {
      url: "/noodle",
      templateUrl: "src/view/partials/menu.html"
    })

    .state("home.tapas", {
      url: "/tapas",
      templateUrl: "src/view/partials/menu.html"
    })

    .state("foodDesc", {
      url: "/foodDesc/:name",
      templateUrl: "src/view/food_desc.html",
      controller: "FoodDescCtrl",
      params: {
        name: null,
        desc: null,
        price: null,
        img: null,
        prevState: null
      }
    })

    .state("shoppingCart", {
      url: "/shoppingCart",
      templateUrl: "src/view/shopping_cart.html",
      controller: "CartCtrl",
      params: {
        prevState: null
      }
    })

    .state("auth", {
      url: "/auth",
      templateUrl: "src/view/auth.html",
      controller: "AuthCtrl"
    })

    .state("checkout", {
      url: "/checkout",
      templateUrl: "src/view/checkout.html",
      controller: "CheckoutCtrl"
    })
})

.constant('LANG', "zh")

// Hard coded for dev purpose.
.value('NAV', {
  en: [{ state: "rice", name: "Rice"}, {state: "noodle", name: "Noodle & Soup"}, {state: "tapas", name: "Tapas"}],
  zh: [{ state: "rice", name: "飯"}, {state: "noodle", name: "面 & 湯"}, {state: "tapas", name: "小菜"}]
})

// Hard coded for dev purpose.
.value('MENU', [
  {
    category: "tapas",
    name: {
      en: "Stired Fried Cabbage",
      zh: "炒高麗菜"
    },
    desc: {
      en: "Lorem Ipsum is simply dummy text of the printing.",
      zh: "炒菜"
    },
    price: 20,
    img: "img/food/stir_fried_cabbage.jpg"
  },

  {
    category: "tapas",
    name: {
      en: "Marinated Tofu",
      zh: "滷豆腐"
    },
    desc: {
      en: "Lorem Ipsum is simply dummy text of the printing.",
      zh: "豆腐"
    },
    price: 10,
    img: "img/food/marinated_tofu.jpg"
  },

  {
    category: "rice",
    name: {
      en: "Pork Chop",
      zh: "豬排"
    },
    desc: {
      en: "Lorem Ipsum is simply dummy text of the printing.",
      zh: "大豬排"
    },
    price: 80,
    img: "img/food/pork_chop.jpg"
  },

  {
    category: "rice",
    name: {
      en: "Shrimp Rolls",
      zh: "蝦捲"
    },
    desc: {
      en: "Lorem Ipsum is simply dummy text of the printing.",
      zh: "炸蝦"
    },
    price: 70,
    img: "img/food/shrimp_rolls.jpg"
  },

  {
    category: "tapas",
    name: {
      en: "Braised Egg",
      zh: "滷蛋"
    },
    desc: {
      en: "Lorem Ipsum is simply dummy text of the printing.",
      zh: "香濃滷蛋"
    },
    price: 10,
    img: "img/food/braised_egg.jpg"
  },

  {
    category: "rice",
    name: {
      en: "Chicken Leg",
      zh: "雞腿"
    },
    desc: {
      en: "Lorem Ipsum is simply dummy text of the printing.",
      zh: "大雞腿"
    },
    price: 90,
    img: "img/food/chicken_leg.jpg"
  },

  {
    category: "rice",
    name: {
      en: "Fried Chicken Cutlet",
      zh: "雞排"
    },
    desc: {
      en: "Lorem Ipsum is simply dummy text of the printing.",
      zh: "大雞排"
    },
    price: 90,
    img: "img/food/fried_chicken_cutlet.jpg"
  },

  {
    category: "noodle",
    name: {
      en: "Fried Chicken Cutlet",
      zh: "雞排"
    },
    desc: {
      en: "Lorem Ipsum is simply dummy text of the printing.",
      zh: "大雞排"
    },
    price: 90,
    img: "img/food/fried_chicken_cutlet.jpg"
  },
]);
