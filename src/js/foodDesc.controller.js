angular.module('betterPOSClientApp.controller.foodDesc', [])
  .controller("FoodDescCtrl", function($scope, $state, $stateParams, CartService) {
    $scope.name = $stateParams.name;
    $scope.desc = $stateParams.desc;
    $scope.price = $stateParams.price;
    $scope.img = $stateParams.img;

    $scope.quantity = 0;

    $scope.plus = function() {
      $scope.quantity++;
    };

    $scope.minus = function() {
      if ($scope.quantity > 0) $scope.quantity--;
    };

    $scope.addToCart = function() {
      if ($scope.quantity > 0) {
        CartService.addToCart({
          name: $scope.name,
          desc: $scope.desc,
          price: $scope.price,
          quantity: $scope.quantity
        });
      }
      console.log($stateParams.prevState);
      $state.go($stateParams.prevState);
    };
  });
