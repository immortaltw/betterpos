angular.module('betterPOSClientApp.controller.cart', [])
  .controller("CartCtrl", function($scope, $state, $stateParams, CartService) {
    $scope.currentItems = CartService.outstandingItems;
    $scope.subtotal = CartService.currentSubtotal;
    $scope.itemAttr = {showRemoveBtn: false};
    $scope.isCartEmpty = ($scope.currentItems.length === 0)? true: false;

    $scope.goToMenu = function() {
      $state.go($stateParams.prevState);
    };

    $scope.goToAuth = function() {
      if ($scope.isCartEmpty) return;
      console.log("auth");
      $state.go("auth");
    };

    $scope.plus = function(item, idx) {
      if($scope.isCartEmpty) $scope.isCartEmpty = false;
      item.quantity++;
      CartService.currentSubtotal += item.price;
      $scope.subtotal = CartService.currentSubtotal;
    };

    $scope.minus = function(item, idx) {
      if ((item.quantity-1) < 0) return;
      else if ((item.quantity-1) === 0 && $scope.currentItems.length === 1) $scope.isCartEmpty = true;

      item.quantity--;
      CartService.currentSubtotal -= item.price;
      $scope.subtotal = CartService.currentSubtotal;
    };

    $scope.edit = function() {
      $scope.itemAttr.showRemoveBtn = !$scope.itemAttr.showRemoveBtn;
    };

    $scope.delete = function(item) {
      CartService.removeFromCart(item);
      $scope.itemAttr.showRemoveBtn = false;
      $scope.subtotal = CartService.currentSubtotal;
      if ($scope.subtotal === 0) $scope.isCartEmpty = true;
    };
  });
