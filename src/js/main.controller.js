angular.module('betterPOSClientApp.controller.main', [])
  .controller("MainCtrl", function($scope, $state, LANG, NAV, MENU, CartService) {
    $scope.menuTitles = NAV[LANG];

    $scope.selected = 0;
    $scope.select = function(idx) {
      $scope.selected = idx;
    };

    $scope.totalItemCount = CartService.totalItemCount;

    $scope.goToCart = function() {
      $state.go("shoppingCart", {prevState: $state.current.name});
    };

    $scope.$watch(function() {return CartService.totalItemCount}, function(newVal, oldVal) {
      $scope.totalItemCount = newVal;
    });
});
