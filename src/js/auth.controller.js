angular.module('betterPOSClientApp.controller.auth', [])
  .controller("AuthCtrl", function($scope, $state, CartService) {
    $scope.currentItems = CartService.outstandingItems;
    $scope.total = CartService.currentSubtotal;
    $scope.showOrderDetail = false;

    $scope.radioModel = 'togo';
});
