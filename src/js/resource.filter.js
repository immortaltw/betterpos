angular.module('betterPOSClientApp.filters', [])
  .filter("menuFilter", function() {
    return function(items, cat) {
      return items.filter(function(elem, idx, arr) {
        return elem.category === cat;
      });
    };
  });
