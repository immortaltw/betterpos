angular.module('betterPOSClientApp.service.cart', [])
 .service("CartService", function(LANG) {
   this.totalItemCount = 0;
   this.currentSubtotal = 0;
   this.outstandingItems = [];

    this.getName = function(item, field) {
      return item? item[field][LANG]: null;
    };

   this.addToCart = function(item) {
     if (!item) return;
     this.totalItemCount += item.quantity;
     this.currentSubtotal += item.quantity*item.price;
     this.outstandingItems.push(item);
   };

   this.removeFromCart = function(item) {
    if (this.outstandingItems.length <= 0) return;
    var removeItemIdx = -1;
    for (var i=0; i<this.outstandingItems.length; ++i) {
      if (this.outstandingItems[i].name === item.name) {
        removeItemIdx = i;
        if ((this.totalItemCount - this.outstandingItems[i].quantity) >= 0) {
          this.totalItemCount -= this.outstandingItems[i].quantity;
        }

        if ((this.currentSubtotal - this.outstandingItems[i].quantity * this.outstandingItems[i].price) >= 0) {
          this.currentSubtotal -= this.outstandingItems[i].quantity * this.outstandingItems[i].price;
        }

        break;
       }
     }
     if (removeItemIdx>0) this.outstandingItems.splice(removeItemIdx, 1);
   }
 });
