angular.module('betterPOSClientApp.controller.menu', [])
  .controller("MenuCtrl", function($scope, $state, MENU, LANG) {
    $scope.menu = MENU;
    $scope.state = $state.current.name.replace("home.", "");

    $scope.getName = function(item, field) {
      return item[field][LANG];
    };

    $scope.goToFoodDesc = function(item) {
      $state.go("foodDesc", {
        name: $scope.getName(item, "name"),
        desc: $scope.getName(item, "desc"),
        price: item.price,
        img: item.img,
        prevState: "home."+$scope.state
      });
    };
});
