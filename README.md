# Cheap POS

## Purpose

This app was built with ionic/angular.js. The purpose of building it is to show that restaurants doesn't necessarily need to buy a whole set of super fancy/expensive POS system just to take orders and keep track of orders created.

With a very simple backend server and a simple app, it's often enough to fit small restaurants' needs.

## Screenshots

There're 3 main views accessible by the tab on the left.

### Take Order View
![take_order.png](https://bitbucket.org/repo/n5o8jA/images/3803883137-take_order.png)

### Order List View
![order_list.png](https://bitbucket.org/repo/n5o8jA/images/3687173526-order_list.png)

### Kitchen Display System View
![KDS.png](https://bitbucket.org/repo/n5o8jA/images/3858323475-KDS.png)
